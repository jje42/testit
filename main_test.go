package main

import "testing"

func Test_testit(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"t1", args{2, 2}, 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := testit(tt.args.x, tt.args.y); got != tt.want {
				t.Errorf("testit() = %v, want %v", got, tt.want)
			}
		})
	}
}
