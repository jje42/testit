package main

import (
	"fmt"
)

func testit(x, y int) int {
	return x * y
}

func main() {
	fmt.Println(testit(2, 2))
}
